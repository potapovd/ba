//= plugins/jquery-3.3.1.min.js
//= plugins/webfontloader.min.js
//= plugins/jquery.finiteStateMachine.min.js
//= plugins/bootstrap/dist/util.js
//= plugins/bootstrap/dist/carousel.js
//= plugins/bootstrap/dist/tab.js
//= plugins/bs-stepper.min.js
//= plugins/jquery.mask.min.js



$('.carousel').carousel()


$("a[href='#top']").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});

$(".price-inp").keyup(function () {
    var val = this.value;
    val = val.replace(/[^0-9\.]/g, '');

    if (val != "") {
        valArr = val.split('.');
        valArr[0] = (parseInt(valArr[0], 10)).toLocaleString('en');
        val = valArr.join('.');
    }

    this.value = val;
});

$('#sm').finiteStateMachine({
    linear: true
});

$(document).ready(function () {
    WebFont.load({
        google: {
            families: ['Montserrat', 'Source Sans Pro']
        }
    });


    $('.dropdown').click(function () {
        $(this).attr('tabindex', 1).focus();
        $(this).toggleClass('active');
        $(this).find('.dropdown-menu').slideToggle(300);
    });
    $('.dropdown').focusout(function () {
        $(this).removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
    });
    $('.dropdown .dropdown-menu li').click(function () {
        $(this).parents('.dropdown').find('span').text($(this).text());
        $(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
    });
    $('.dropdown-menu li').click(function () {
        console.log($(this).parents('.dropdown').find('input').val())
    });

    $('#f10').mask('00/00/0000');

    var stepperEl = $('.bs-stepper')[0];
    var stepper = new Stepper(stepperEl,{
        //linear: false,
        animation: true
    })
    $("#form-next-step").on('click',function (e) {
        e.preventDefault();
        stepper.next()
        //$('.bs-stepper-header .step').removeClass('active')
        $('.bs-stepper-header').find('.step.active').prevAll().addClass('active-custom')
    })
    $("#form-prev-step").on('click',function (e) {
        e.preventDefault();
        stepper.previous()
        //$('.bs-stepper-header').find('.step.active').prevAll().addClass('active')
        $('.bs-stepper-header').find('.step.active').nextAll().removeClass('active-custom')
    })

    stepperEl.addEventListener('show.bs-stepper', function (event) {
        console.warn(event.detail.indexStep)
        if(event.detail.indexStep==3){
            $("#form-next-step").hide();
            $("#send-button-action").show();
        }else{
            $("#form-next-step").show();
            $("#send-button-action").hide();
        }
    })

    $("#send-button-action").on('click',function (e) {
        e.preventDefault();
        /* SENDING DATA ACTION */
        console.log("SENDING DATA...")
        //after sending
        $('.last-step-visible').toggle()
        $('.last-step-invisible').toggle()
        $("#form-prev-step, #form-next-step, #send-button-action, .bs-stepper-header").hide()
    })








    window.onload = function () {
        // if (window.jQuery) {
        // 	// jQuery is loaded
        // 	alert("Yeah!");
        // } else {
        // 	// jQuery is not loaded
        // 	alert("Doesn't Work");
        // }
    }


});